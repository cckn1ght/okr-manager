import api from "./APIUtils";
import {OkrEntity} from "../constants/payload";

export const getOwnerOkrs = (ownerId: string): Promise<OkrEntity[]> => {
    return api.get<OkrEntity[]>("/okr/by-owner",
        {params: {owner_id: ownerId}})
        .then(r => r.data)
};

export const createOkr = (okr: OkrEntity): Promise<OkrEntity> => {
    return api.post<OkrEntity>("/okr", okr)
        .then(r => r.data);
};
export const updateOkr = (id: string, okr: OkrEntity): Promise<OkrEntity> => {
    return api.put<OkrEntity>(`/okr/${id}`, okr)
        .then(r => r.data);
};
export const deleteOkrCall = (id: string): Promise<void> => {
    return api.delete(`/okr/${id.toString()}`);
};

export const getOkrById = (id: string): Promise<OkrEntity> => {
    return api.get<OkrEntity>(`/okr/${id}`)
        .then(r => r.data)
};

import React, {useCallback, useEffect, useState} from 'react';
import {OkrEntity, UserEntity} from "../constants/payload";
import {deleteOkrCall, getOwnerOkrs} from "../util/OkrApi";
import {Button, notification} from "antd";
import {Link, useParams} from "react-router-dom";
import {APP_NAME} from "../constants";
import {getUserById} from "../util/UserApi";
import {OkrList} from "./OkrList";

export interface CurrentUserProps {
    currentUser: UserEntity
}

export const OkrView: React.FC<CurrentUserProps> = (props: CurrentUserProps) => {
    const [okrs, setOkrs] = useState<OkrEntity[]>([]);
    const {user_id} = useParams<{ user_id: string }>();
    const [loading, setLoading] = useState(true);
    const [targetUser, setTargetUser] = useState<UserEntity | null>(null);
    const user = props.currentUser;
    const isOwner = user_id === user.id;
    useEffect(() => {
        reload()
    }, []);
    const reload = useCallback(() => {
        (async () => {
            setLoading(true);
            const res = await getOwnerOkrs(user_id);
            setOkrs(res);
            if (!isOwner) {
                const targetU = await getUserById(user_id);
                setTargetUser(targetU);
            }
            setLoading(false);
        })()
    }, [user_id]);
    const deleteOkr = async (id: string) => {
        try {
            await deleteOkrCall(id);
            reload();
        } catch (e) {
            notification.error({
                message: APP_NAME,
                description: 'some thing went wrong! Please try again!'
            });
        }
    };
    return (
        <div style={{marginTop: 70}}>
            {(!isOwner && targetUser) && <h2 style={{textAlign: "center"}}>{targetUser.name}</h2>}
            <OkrList okrs={okrs} isLoading={loading} editable={isOwner} onDelete={deleteOkr}/>
            {
                isOwner &&
                <Button type="primary" style={{margin: "auto", display: "block", marginBottom: 30}}>
                    <Link to="/user/okr_adder">
                <span style={{width: 150}}>
                新增 OKR
                </span>
                    </Link>
                </Button>
            }
        </div>
    );
};

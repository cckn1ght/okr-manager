import api from "./APIUtils";
import {ApiResponse, JwtAuthenticationResponse, LoginRequest, SignUpRequest, UserEntity} from "../constants/payload";

export const login = (loginRequest: LoginRequest): Promise<JwtAuthenticationResponse> => {
    return api.post<JwtAuthenticationResponse>("/auth/login", loginRequest)
        .then(r => r.data)
};
export const getCurrentUser = (): Promise<UserEntity> => {
    return api.get<UserEntity>("/auth/status")
        .then(r => r.data)
};

export const signup = (signUpRequest: SignUpRequest): Promise<ApiResponse> => {
    return api.post<ApiResponse>("/user/register", signUpRequest)
        .then(r => r.data)
};
export const getUserById = (id: string): Promise<UserEntity> => {
    return api.get<UserEntity>(`/user/${id}`)
        .then(r => r.data)
};

export const getUserCount = (): Promise<number> => {
    return api
        .get<number>("/user/count")
        .then(r => r.data)
};
export const getUserByPage = (page: number, size: number): Promise<UserEntity[]> => {
    return api.get<UserEntity[]>("/user", {params: {page, size}}).then(r => r.data)
};

export const getAllUsers = (): Promise<UserEntity[]> => {
    return api.get<UserEntity[]>("/user").then(r => r.data)
};

import React, {useEffect, useState} from "react";
import {Button, Input, List, Skeleton} from "antd";
import {UserEntity} from "../../constants/payload";
import {getAllUsers} from "../../util/UserApi";
import {RouteComponentProps, withRouter} from "react-router";
import * as _ from 'lodash';

const { Search } = Input;

interface UserListProps {
    selectedUserId: string | null
    setUserId: (n: string) => void
}

const UserListWithRouter: React.FC<UserListProps & RouteComponentProps> =
    (props: UserListProps & RouteComponentProps) => {
    const PAGE_SIZE = 10;
    const [loading, setLoading] = useState(true);
    const [users, setUsers] = useState<UserEntity[]>([]);
    const [visibleUsers, setVisibleUsers] = useState<UserEntity[]>([]);
    useEffect(() => {
        let mounted = true;
        (async () => {
            setLoading(true);
            const allUsers = await getAllUsers();
            if (mounted) {
                setUsers(allUsers);
                setVisibleUsers(allUsers);
                setLoading(false);
            }
        })();
        return () => {
            mounted = false;
            return
        }
    }, []);
    const onSearch = (value: string) => {
        _.debounce(() => {
            const filterUsers = _.filter<UserEntity>(users,
                (u) => u.name.indexOf(value) !== -1 || u.username.indexOf(value) !== -1);
            setVisibleUsers(filterUsers);
        }, 200)()
    };
    return (
        <div>
            <h2 style={{margin: "auto", textAlign: "center", marginTop: 60}}>用户列表</h2>
            <br />
            <div
                style={{margin: "auto", alignContent: "center"}}
            >
                <span style={{marginLeft: 10, fontSize: "20px"}}><b>搜索用户</b></span>
                <Search
                    placeholder="搜索用户名或邮箱"
                    // onSearch={value => onSearch(value)}
                    onChange={e => onSearch(e.target.value)}
                    style={{ width: 300, margin: 20 }}
                />
                <List
                    bordered
                    className="user-list"
                    itemLayout="horizontal"
                    dataSource={visibleUsers}
                    pagination={{position: "bottom",
                        defaultCurrent: 1,
                        pageSize: PAGE_SIZE,
                        total: users.length + 1
                    }}
                    style={{backgroundColor: "white"}}
                    renderItem={item => (
                        <List.Item key={item.id}>
                            <Button
                                block={true}
                                type={"default"}
                                className={"user-box " + ((item.id === props.selectedUserId) && "selected-user-box")}
                                 onClick={() => props.setUserId(item.id)} style={{width: "100%"}}>
                            <Skeleton avatar title={false} loading={loading} active>
                                <List.Item.Meta
                                    title={<span style={{fontSize: "20px"}}>{item.name}</span>}
                                    description={item.username}
                                />
                            </Skeleton>
                            </Button>
                        </List.Item>
                    )}
                />
            </div>
        </div>
    )
};
export const UserList = withRouter(UserListWithRouter);

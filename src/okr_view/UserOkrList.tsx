import React, {useCallback, useEffect, useState} from "react";
import {UserList} from "../user/user_list/UserList";
import {OkrEntity} from "../constants/payload";
import {Layout, notification} from "antd";
import {OkrList} from "./OkrList";
import {deleteOkrCall, getOwnerOkrs} from "../util/OkrApi";
import {APP_NAME} from "../constants";
import "./index.css"
const {Sider, Content} = Layout;
export const UserOkrList: React.FC = () => {
    const [userId, setUserId] = useState<string | null>(null);
    const [okrs, setOkrs] = useState<OkrEntity[]>([]);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        reload()
    }, [userId]);
    const reload = useCallback(() => {
        (async () => {
            if (userId) {
                setLoading(true);
                const res = await getOwnerOkrs(userId);
                setOkrs(res);
                setLoading(false);
            }
        })()
    }, [userId]);
    const deleteOkr = async (id: string) => {
        try {
            await deleteOkrCall(id);
            reload();
        } catch (e) {
            notification.error({
                message: APP_NAME,
                description: 'some thing went wrong! Please try again!'
            });
        }
    };
    return (
        <Layout>
            <Sider width={450} style={{ background: '#fff' }}>
                <UserList setUserId={setUserId} selectedUserId={userId}/>
            </Sider>
            <Content>
                {userId && <OkrList okrs={okrs} onDelete={deleteOkr} editable={false} isLoading={loading}/>}
            </Content>
        </Layout>
    )
};
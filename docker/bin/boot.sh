#!/bin/sh
set -x
export CONF_DIR=/etc/okr/conf

rm -f /etc/nginx/conf.d/default.conf
ln -s ${CONF_DIR}/default.conf /etc/nginx/conf.d/default.conf

nginx -g "daemon off;"

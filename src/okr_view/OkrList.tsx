import React from 'react';
import {OkrEntity} from "../constants/payload";
import {OkrDetail} from "./OkrDetail";
import {Divider, Skeleton} from "antd";

export interface OkrListProps {
    okrs: OkrEntity[]
    isLoading: boolean
    editable: boolean
    onDelete: (id: string) => void
}

export const OkrList: React.FC<OkrListProps> = (props: OkrListProps) => {
    const {okrs, isLoading, editable, onDelete} = props;
    return (
        <div style={{marginTop: 70}}>
            {isLoading ?
                <Skeleton active={true} paragraph={{rows: 4}}/> :
                okrs.length === 0 ?
                    <h3 style={{textAlign: "center", marginTop: 300}}>该用户还没有填写 OKR</h3> :
                    okrs.map(okr => (
                        <div key={okr.id!}>
                            <OkrDetail okr={okr} onDelete={() => onDelete(okr.id!)} editable={editable}/>
                            <Divider dashed/>
                        </div>
                    ))}
        </div>
    );
};

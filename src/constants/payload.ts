export interface LoginRequest {
    email: string,
    password: string
}
export interface SignUpRequest extends LoginRequest{
    name: string,
}
export interface JwtAuthenticationResponse {
    accessToken: string;
    tokenType: string;
}
export interface ApiResponse {
    success: boolean;
    message: string;
}
export interface UserEntity {
    id: string;
    name: string;
    username: string;
    email: string;
}

export interface OkrEntity {
    id?: string;
    owner_id: string;
    owner_name: string;
    title: string;
    obj: string;
    krs: string[];
    created_at?: number;
}
export interface KrEntity {
    id?: string;
    okr_id?: string;
    content: string;
    created_at?: number;
}